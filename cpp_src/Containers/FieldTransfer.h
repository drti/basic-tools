
#pragma once

#include <Containers/UnstructuredMesh.h>
#include <Containers/ElementFilter.h>

#include <FE/DofNumbering.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>

#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;
typedef bg::model::point<BasicTools::CBasicFloatType, 3, bg::cs::cartesian> point;
typedef bg::model::segment<point> segment;
//CBasicFloatType
//typedef bg::model::box<point> box;
//typedef std::pair<box, BasicTools::CBasicIndexType> value;
typedef std::pair<point, BasicTools::CBasicIndexType> value;
typedef std::pair<segment, std::pair<BasicTools::CBasicIndexType,BasicTools::CBasicIndexType> > sValue;

namespace BasicTools {

enum TransferMethods {
    Nearest=0,
    Interp,
    Extrap,
    Clamp,
    ZeroFill
};

class TransferClass {
public:
    TransferClass();
    std::string ToStr();
    void SetVerbose(bool verbose=false);
//    void SetSourceFEField();
    void SetSourceMesh(UnstructuredMesh* sourceMesh);
    void SetSourceSpace(const std::string& space);
    void SetSourceNumbering(DofNumbering* numbering);

    void SetTransferMethod(const std::string& method);
    std::string GetTransferMethod();
//
    void SetElementFilter(const ElementFilterEvaluated& filter);
    MAPSETGET_MatrixDDD(TargetPoints, targetPoints)

    void Compute();
//  GetProjectorOperator()
    std::vector<CBasicIndexType> rows;
    std::vector<CBasicIndexType> cols;
    std::vector<CBasicFloatType> data;
    MatrixID1 status;
    MatrixID1& GetStatus();

    CBasicIndexType nb_source_Dofs;
    CBasicIndexType nb_targetPoints;
    bool useEdges = true;

private:
    bool verbose;
    int insideMethod;
    int outsideMethod;
    UnstructuredMesh* sourceMesh;
    Space sourceSpace;
    DofNumbering* sourceNumbering;
    ElementFilterEvaluated elementfilter;
    bool elementFilterSet;
    std::shared_ptr<MapMatrixDDD > targetPoints;

    bgi::rtree<value, bgi::quadratic<16>> nodeRTree;
    bgi::rtree<value, bgi::quadratic<16>> centerRTree;
    bgi::rtree<sValue, bgi::quadratic<16>> segmentRTree;
    MatrixIDD dualGraph;
    MatrixID1 usedPoints;
    MatrixDDD cellsCenters;
    //void ComputeBarycentricCoordinateOnElement(){}
    std::pair<ElementsContainer,int> GetElement(int enb);
};

} // namespace BasicTools
